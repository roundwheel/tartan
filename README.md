# tartan XML
**T**ool for **A**utomating the **R**epetitive **T**ask of **A**dding **N**amespaces to XML

## To install in the local Maven repository

```
mvn install
```
This creates the necessary artifacts in ~/.m2/repository/ca/roundwheel/tartan/

## To use tartan XML

### Using Maven

```
<dependency>
    <groupId>ca.roundwheel</groupId>
    <artifactId>tartan</artifactId>
    <version>1.1</version>
</dependency>
```

### Using Gradle
```
compile group: 'ca.roundwheel', name: 'tartan', version: '1.1'
```

## To generate the JAR file

```
mvn package
```

This creates the JAR file ./target/tartan-{VERSION}.jar

## Invoking tartan XML

### Locally

Call this method:

```
process(String xml, String jaxbFileName)
```

Where _xml_ contains the contents of the XML to convert, and _jaxbFileName_ is the full 
path to the JAXB jar file that describes all of the possible element types contained within 
the XML.

### In a web app

```
process(String xml, List<String> remoteLibraries)
```

Where _xml_ contains the contents of the XML to convert, and _remoteLibraries_ contains a 
list of URLs for the JAXB jar(s) that describe all of the possible element types contained 
within the XML.