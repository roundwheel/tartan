package ca.roundwheel.tools.xml;

import java.net.URLClassLoader;
import java.util.Set;

import javax.xml.bind.annotation.XmlType;

import org.reflections.Reflections;
import org.reflections.scanners.FieldAnnotationsScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * {@code PackageInspector} scans the entire contents of a Java package and its sub-packages and looks for
 * classes that are annotated with {@code @XmlType}.  It builds a map of all XML types and their package
 * or namespace.
 * 
 * This class uses the <a href="https://github.com/ronmamo/reflections">org.reflections</a> library.
 * 
 * @author Yvon Comeau
 */
public class PackageInspector {

	static final Logger log = LoggerFactory.getLogger(PackageInspector.class);

	/**
	 * Scans certain packages and returns all classes annotated with the JAXB {@code XmlType} annotation.
	 * 
	 * @return  all JAXB classes
	 * @throws TartanServiceException 
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Set<Class<?>> findXmlTypes(URLClassLoader jaxbClassLoader) throws TartanServiceException {
			
		Reflections reflections = new Reflections(new ConfigurationBuilder()
				.addUrls(jaxbClassLoader.getURLs()) 
				.addClassLoader(jaxbClassLoader)
				.setScanners(new SubTypesScanner(), 
	                  new TypeAnnotationsScanner(),
	                  new FieldAnnotationsScanner())
				.filterInputsBy(new FilterBuilder()));
		
		Set<Class<?>> annotated = reflections.getTypesAnnotatedWith(XmlType.class);
		
		for (Class clazz: annotated) {
			log.debug(clazz.getName());
			
			Set<Class<?>> subTypes = reflections.getSubTypesOf(clazz);
			
			for (Class<?> cls: subTypes) {
				log.debug("Found the following XML Types:\n\t\t" + cls.getName());
			}
		}
		
		return annotated;
	}
}