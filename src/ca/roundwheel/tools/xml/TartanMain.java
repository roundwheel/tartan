package ca.roundwheel.tools.xml;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The entry point to the XML conversion utility.
 * 
 * @author Yvon Comeau
 */
public class TartanMain implements Tartan {

	static final Logger log = LoggerFactory.getLogger(TartanMain.class);

	/**
	 * Creates a new instance of {@code TartanMain}.
	 */
	public TartanMain() {
		
	}
	
	/* (non-Javadoc)
	 * @see ca.roundwheel.tools.xml.Tartan#process(java.lang.String, java.lang.String)
	 */
	@Override
	public String process(String xml, String jaxbFileName) throws TartanServiceException {
		
		URLClassLoader jaxbClassLoader = loadJaxbLibraryClassloader(jaxbFileName);
		
		NamespaceMap namespaceMap = loadNamespaceMap(jaxbClassLoader);

		log.debug("--------------------------------------------");
		log.info(" Fixing namespaces  ");
		log.debug("--------------------------------------------");
		
		XmlNamespaceFixer xmlFixer = new XmlNamespaceFixer(namespaceMap);
		
		String namespacedXml = xmlFixer.tagXmlWithNamespaces(xml);
		
		log.debug("namespacedXml =" + namespacedXml);
		log.debug("--------------------------------------------");
		
		return namespacedXml;
	}
	
	/* (non-Javadoc)
	 * @see ca.roundwheel.tools.xml.Tartan#process(java.lang.String, java.util.List)
	 */
	@Override
	public String process(String xml, List<String> remoteLibraries) throws TartanServiceException {
		
		URLClassLoader jaxbClassLoader = loadJaxbLibraryClassloader(remoteLibraries);
		
		NamespaceMap namespaceMap = loadNamespaceMap(jaxbClassLoader);

		log.debug("--------------------------------------------");
		log.info(" Fixing namespaces  ");
		log.debug("--------------------------------------------");
		
		XmlNamespaceFixer xmlFixer = new XmlNamespaceFixer(namespaceMap);
		
		String namespacedXml = xmlFixer.tagXmlWithNamespaces(xml);
		
		log.debug("namespacedXml =" + namespacedXml);
		log.debug("--------------------------------------------");
		
		return namespacedXml;
	}

	/**
	 * Creates a class loader from a List of URLs pointing to remote JAXB jar files.
	 * 
	 * @param remoteLibraries a list of JAR file URLs
	 * @return  a {@code URLClassLoader that includes all the remote libraries}
	 * @throws TartanServiceException
	 */
	private URLClassLoader loadJaxbLibraryClassloader(List<String> remoteLibraries) throws TartanServiceException {
		URL[] jaxbURLs = new URL[remoteLibraries.size()];
		log.info("Loading JAXB Library Classloader from remote libraries");
		
		try {
			for (int i=0; i< remoteLibraries.size(); i++) {
				URL url = new URL(remoteLibraries.get(i));
				jaxbURLs[i] = url;
			}
		} catch (MalformedURLException e) {
			throw new TartanServiceException(e);
		}
		return new URLClassLoader(jaxbURLs, this.getClass().getClassLoader());
	}

	/**
	 * Creates a classloader from the String pointing to the JAXB jar/zip file.
	 * 
	 * @param jaxbFileName  the local JAXB JAR file
	 * @return  a {@code URLClassLoader that includes the local JAR file}
	 * @throws TartanServiceException
	 */
	private URLClassLoader loadJaxbLibraryClassloader(String jaxbFileName) throws TartanServiceException {
		File jaxbLibrary = new File(jaxbFileName);
		log.info("Loading JAXB Library Classloader");
		
		URL[] jaxbURLs;
		try {
			jaxbURLs = new URL[] {jaxbLibrary.toURI().toURL()};
		} catch (MalformedURLException e) {
			throw new TartanServiceException(e);
		}
		return new URLClassLoader(jaxbURLs, this.getClass().getClassLoader());
	}

	/**
	 * Searches through the JAR file for all classes tagged with JAXB annotations and ads them to the
	 * namespace map.
	 * 
	 * @param  jaxbClassLoader  the {@code URLClassLoader that includes the JAXB JAR(s)}
	 * @return  the {@NamespaceMap} object containing all JAXB classes that describe the XML we are converting
	 * @throws TartanServiceException
	 */
	private NamespaceMap loadNamespaceMap(URLClassLoader jaxbClassLoader) throws TartanServiceException {
		PackageInspector inspector = new PackageInspector();

		log.debug("--------------------------------------------");
		log.info(" Finding all XML types... ");
		log.debug("--------------------------------------------");
		
		Set<Class<?>> xmlClasses = inspector.findXmlTypes(jaxbClassLoader);

		log.debug("--------------------------------------------");
		log.info(" Found " + xmlClasses.size() + " XML classes" );

		InspectorGadget inspectorGadget = new InspectorGadget(jaxbClassLoader);
		NamespaceMap namespaceMap = new NamespaceMap(xmlClasses, inspectorGadget);
		log.info("Added {} namespaces to the namespace map", namespaceMap.size());
		
		return namespaceMap;
	}

	/**
	 * Main... Can be used for testing as a standalone app.
	 * 
	 * @param args
	 * @throws TartanServiceException 
	 */
	public static void main (String ... args) throws TartanServiceException {
		TartanMain tartanMain = new TartanMain();
		tartanMain.process();
	}
	
	/**
	 * This can be used to run Tartan as a standalone application.
	 * 
	 * @throws TartanServiceException 
	 */
	private void process() throws TartanServiceException {
		try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("./resources/sample-out.xml"))) {
			String xml = readXmlFile();
			String result = process(xml, "");
			bufferedWriter.write(result);
		} catch (IOException e) {
			throw new TartanServiceException(e);
		}
	}
	
	/**
	 * Read a local XML file. Used when testing as a standalone app.
	 * 
	 * @return  the contents of the file as a String
	 * @throws IOException
	 */
	private String readXmlFile() throws IOException {
		File xmlFile = new File("./resources/sample.xml");
		String xml = FileUtils.readFileToString(xmlFile, "UTF-8");
		return xml;
	}
}