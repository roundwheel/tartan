package ca.roundwheel.tools.xml;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * {@code NamespaceMap} is a wrapper to the data structure that holds information about each of the JAXB 
 * classes and what namespace should be used to reference that type in an XML document.
 * 
 * @author Yvon Comeau
 */
public class NamespaceMap implements Serializable {

	private static final long serialVersionUID = 6135568699914121743L;

	static final Logger log = LoggerFactory.getLogger(NamespaceMap.class);
	
	/**
	 * All JAXB types found in the source directory.
	 */
	private Set<Class<?>> xmlClasses;
	
    /**
     * Converts the name of the java package into a standard XML namespace URI
     */
    private transient PackageToNamespaceConverter packageConverter = new PackageToNamespaceConverter();

    /**
     * This is the top most level class of the XML input file.
     */
    private transient InspectorGadget inspectorGadget;
    
	/**
	 * Creates a new instance of the {@code NamespaceMap} type by seeding it with the set of JAXB classes
	 * found in the source directory.
	 * 
	 * @param xmlClasses
	 */
	public NamespaceMap(Set<Class<?>> xmlClasses, InspectorGadget inspectorGadget) {
		this.xmlClasses = xmlClasses;
		this.inspectorGadget = inspectorGadget;
	}
	
	/**
	 * The container used to hold the namespace data.  The keys contain the names of all objects, their 
	 * properties, and their sub-properties (etc) in the format:
	 * <p/>
	 * {@code ParentClass.childClass.grandChildClass}
	 * <p/>
	 * For example:
	 * <p/>
	 * WorkOrder.businessInteraction.characteristicSpecification
	 * <p/>
	 * The values are standard namespace URIs derived from the package of each of the JAXB objects.  For 
	 * example:
	 * <p/>
	 * {@code http://ibm.com/telecom/v8_5_0/businessobject/system/sid_v12_5/common}
	 */
	private Map<String, String> namespaceMap = new HashMap<String, String>();

	/**
	 * Returns the namespace URI to use given a path in the form:
	 * <p/>
	 * {@code ParentClass.childClass.grandChildClass}
	 *  
	 * @param objectPath  the full path to the object for which we need a namespace URI
	 * @return  the namespace URI
	 * @throws TartanServiceException 
	 */
	public String findRootURI(String objectPath) throws TartanServiceException {
		String path = normalize(objectPath);
		
		log.debug("Looking for " + path);
		String uri = namespaceMap.get(path);
		if (uri == null) {

			log.debug("Root element not found in namespace map.  Looking for that object type.");
			
			List<Class<?>> matchingXmlClasses = new ArrayList<>();
			for (Class<?> cls: xmlClasses) {
				if (cls.getName().toLowerCase().endsWith(path)) {
					matchingXmlClasses.add(cls);
				}
			}
			
			if (matchingXmlClasses.size() < 1) {
				throw new TartanServiceException("The input file contains a root element of '" + objectPath + "' which"
						+ " does not have a corresonding JAXB class in the source folder.");
			} else if (matchingXmlClasses.size() > 1) {
				throw new TartanServiceException("The input file's root element of '" + objectPath + "' is ambiguous."
						+ " This is not yet supported.");
			}
			
			Class<?> rootClass = matchingXmlClasses.get(0);
			
			uri = packageConverter.convertPackageToNamespace(rootClass.getPackage().getName());

			inspectorGadget.setRootObject(rootClass);
			
			log.debug("Adding root path " + path + ":" + uri + " to the namespace map");
			
			this.put(path, uri);
	    	
			return uri;
		} else {
			log.debug("Found " + uri);
			return uri;
		}
	}
	
	/**
	 * Uses the dot separated notation representing an XML object path to find the corresponding object's namespace URI.
	 * The input might look something like {@code WorkOrder.WorkOrderExtension.ReferencedWorkOrder.ID}.
	 * 
	 * @param elementPath  the dot separated path to an XML element
	 * 
	 * @return
	 */
	public String findNamespaceURI(String elementPath) {
		String path = normalize(elementPath);
		
		log.debug("looking for " + path);
		String uri = namespaceMap.get(path);
		if (uri == null) {
			log.debug("Not found in namespace map.  Looking for that object type.");
			Class<?> objectClass = inspectorGadget.findClass(elementPath);
			
			if (objectClass == null) {
				log.warn("Could not find the class for element with path " + path);
			} else {
				uri = packageConverter.convertPackageToNamespace(objectClass.getPackage().getName());
				log.debug("Adding " + path + ":" + uri + " to the namespace map");
				this.put(path, uri);
			}
		}
		
		return uri;
	}

	/**
	 * Adds the key/value pair to the namespace map.
	 * 
	 * @param xmlElementKey
	 * @param xmlUri
	 */
	private void put(String xmlElementKey, String xmlUri) {
		String key = normalize(xmlElementKey);
		this.namespaceMap.put(key, xmlUri);
		log.debug(key + " -> " + xmlUri + " ... " + this.namespaceMap.size());
	}

	/**
	 * Converts the object path in to a consistently formatted string so that differences in case are ignored.
	 * 
	 * @param elementPath  
	 * 
	 * @return
	 */
	private String normalize(String elementPath) {
		if (elementPath != null) {
			return elementPath.toLowerCase();
		}
		return null;
	}
	
	/**
	 * Returns the number of unique namespaces added to this collection.
	 * 
	 * @return
	 */
	public int size() {
		if (xmlClasses != null) {
			return xmlClasses.size();
		}
		return 0;
	}
}