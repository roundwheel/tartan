package ca.roundwheel.tools.xml;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URLClassLoader;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.xml.datatype.XMLGregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * {@code InspectorGadget} is used for recursively traversing an object's members and converting
 * them to key/value pairs.
 * 
 * @author Yvon X Comeau
 */
public class InspectorGadget  {

	static final Logger log = LoggerFactory.getLogger(InspectorGadget.class);
    
    /**
     * This is the top most level of the XML input file.
     */
    private transient Object rootObject;
    
    /**
     * The classloader containing all the JAXB classes.
     */
    private URLClassLoader jaxbClassLoader;
    
	/**
	 * The string used to demarcate recursive object properties.  For example an object containing 
	 * getPerson().getName().getFirstName() would result in a key Person.Name.FirstName. 
	 */
	private static final String OBJECT_SEPERATOR = ".";
	
	/**
	 * Creates a new instance of an {@code InspectorGadget} object using the specified classloader.
	 *  
	 * @param jaxbClassLoader  the classloader containing all the JAXB classes.
	 */
	public InspectorGadget(URLClassLoader jaxbClassLoader) {
		this.jaxbClassLoader = jaxbClassLoader;
	}
	
	/**
	 * Sets the {@code InspectorGadget}'s root object.
	 * 
	 * @param cls  the JAXB class that is the top most element in the XML input file
	 */
	public void setRootObject(Class<?> cls) {
		try {
			Class<?> rootClass = jaxbClassLoader.loadClass(cls.getName());
			Constructor<?> ctor = rootClass.getConstructor();
			rootObject = ctor.newInstance();
		} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException
				| IllegalArgumentException | InvocationTargetException | ClassNotFoundException e) {

			e.printStackTrace();
			throw new IllegalStateException("Could not instatiate root object " + cls.getName());
		}
	}
	
	/**
	 * Looks to see if 
	 * @param objectPath
	 * @return
	 */
	public Class<?> findClass(String objectPath) {
		return recursiveFindClass(objectPath, rootObject);	
	}
	
	/**
	 * This recursively traverses down the object graph and creates new instance of each property to determine the type
	 * and package of each subsequent child property.
	 * 
	 * @param objectPath  the dot notation object 
	 * @param topLevelObject
	 * 
	 * @return
	 */
	private Class<?> recursiveFindClass(String objectPath, Object topLevelObject) {
		int firstPeriodPosition = objectPath.indexOf(OBJECT_SEPERATOR);
		if (firstPeriodPosition <= 0) {
			// We are done
			return topLevelObject.getClass();
		}
		String pathRemainder = objectPath.substring(firstPeriodPosition + 1, objectPath.length());
		String[] objectProperties = pathRemainder.split("\\.");
		Map<String, Class<?>> properties = getProperties(topLevelObject);
		
		Class<?> lastClass = null;
		
		if (objectProperties.length > 0) {
			// We only care about the first property right now
			String topLevelProperty = objectProperties[0].toLowerCase();
			if (properties.containsKey(topLevelProperty)) {
				Class<?> propertyClass = properties.get(topLevelProperty);

				if (isKnownType(propertyClass)) {
        				return propertyClass;
				} else if (Collection.class.isAssignableFrom(propertyClass)) {
					return convertCollection(pathRemainder, topLevelObject.getClass(), objectProperties[0]);
				}
				
				Object propertyObject = instantiate(propertyClass);
				lastClass = recursiveFindClass(pathRemainder, propertyObject);
			}
			
		}
		
		return lastClass;
		
	}

	/**
	 * Dynamically creates a new instance of an object.
	 * 
	 * @param propertyClass  the class to instantiate
	 * @return  a new object instance
	 */
	private Object instantiate(Class<?> propertyCls) {
		Object propertyObject = null;
		try {
			Class<?> propertyClass = jaxbClassLoader.loadClass(propertyCls.getName());
			propertyObject = propertyClass.newInstance();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			log.warn("Could not create new " + propertyCls.getName() + " instance");
			e.printStackTrace();
		}
		return propertyObject;
	}
	
	/**
	 * This constructs a key/value map of an object's fields and standard java bean members, and the type
	 * of those fields/members.
	 * 
	 * @param object  the object to inspect
	 * 
	 * @return the map describing this object
	 */
	private Map<String, Class<?>> getProperties(Object object) {
        Map<String, Class<?>> propertyMap = getFields(object);
        // getters take precedence in case of any name collisions
        propertyMap.putAll(getGetterMethods(object));
        return propertyMap;
    }

    /**
	 * This constructs a key/value map of an object's standard java bean members, and the type of those
	 * members.
     * 
	 * @param object  the object to inspect
	 * 
	 * @return the map describing this object
     */
    private Map<String, Class<?>> getGetterMethods(Object object) {
        Map<String, Class<?>> result = new HashMap<String, Class<?>>();
        BeanInfo info;
        try {
            info = Introspector.getBeanInfo(object.getClass());
            for (PropertyDescriptor pd : info.getPropertyDescriptors()) {
                Method reader = pd.getReadMethod();
                if (reader != null) {
                    String name = pd.getName();
                    if (!"class".equals(name)) {
                        try {
                            Class<?> clazz = pd.getPropertyType();
                            //log.debug("name=" + name + " , value=" + value);
                            result.put(name.toLowerCase(), clazz);
                        } catch (Exception e) {
                           // FIXME - do something here
                        }
                    }
                }
            }
        } catch (IntrospectionException e) {
            // you can choose to do something here
        }

        return result;
    }

    /**
	 * This constructs a key/value map of an object's fields and the type of those fields.
	 * 
	 * @param object  the object to inspect
	 * 
	 * @return the map describing this object
     */
    private Map<String, Class<?>> getFields(Object object) {
        return getFields(object, object.getClass());
    }

    /**
	 * This constructs a key/value map of an object's fields and the type of those fields.
	 * 
	 * @param object  the object to inspect
	 * 
	 * @return the map describing this object
     */
    private Map<String, Class<?>> getFields(Object object, Class<?> classType) {
        Map<String, Class<?>> result = new HashMap<String, Class<?>>();

        Class<?> superClass = classType.getSuperclass();
        if (superClass != null) { 
        		result.putAll(getFields(object, superClass));
        }

        // get public fields only
        Field[] fields = classType.getFields();
        for (Field field : fields) {
            result.put(field.getName().toLowerCase(), field.getClass());
        }
        return result;
    }

    /**
     * This determines what type of object is stored in a collection declared using generic notation.
     * 
     * @param objectPath  the dot notation location of this object in the XML/JAXB structure
     * @param listClass  the List object we are analyzing
     * @param property  the containing class' property we are inspecting
     * 
     * @return  the type contained within the collection
     */
    private Class<?> convertCollection(String objectPath, Class<?> listClass, String property) {

		Field listField = null;
		String newProperty = null;
			
		try {
			listField = listClass.getDeclaredField(property);
		} catch (NoSuchFieldException | SecurityException e) {
			try {
				newProperty = property.substring(0, 1).toLowerCase() + property.substring(1, property.length());
				listField = listClass.getDeclaredField(newProperty);
			} catch (NoSuchFieldException | SecurityException e1) {
				System.err.println(objectPath + " property '" + property + "' which does not play well with collections. Also tried '" + newProperty + "'");
			}
		}
		
		if (listField != null) {
			ParameterizedType stringListType = (ParameterizedType) listField.getGenericType();
			Class<?> listType = (Class<?>) stringListType.getActualTypeArguments()[0];
			if (isKnownType(listType)) {
				return listType;
			} else {
				Object listObject = instantiate(listType);
				return recursiveFindClass(objectPath, listObject);
			}
		}
		
		return null;
    }
    
	/**
	 * This is used to determine whether we can stop the recursive crawl and tag the given property as a 
	 * known / standard schema type.
	 * 
	 * @param propertyClass  the Class of the property we are inspecting
	 * 
	 * @return {@code true} if this Class is a known type, {@code false} otherwise
	 */
	private boolean isKnownType(Class<?> propertyClass) {
		return Double.class.isAssignableFrom(propertyClass) 
			|| Integer.class.isAssignableFrom(propertyClass) 
			|| Boolean.class.isAssignableFrom(propertyClass)
			|| String.class.isAssignableFrom(propertyClass)
			|| BigInteger.class.isAssignableFrom(propertyClass)
			|| BigDecimal.class.isAssignableFrom(propertyClass)
			|| XMLGregorianCalendar.class.isAssignableFrom(propertyClass);
	}
}