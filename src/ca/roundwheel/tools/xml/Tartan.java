package ca.roundwheel.tools.xml;

import java.util.List;

/**
 * Tartan, the Tool for Automating the Repetitive Task of Adding Namespaces to XML takes converts XML that is 
 * not prefixed and automatically prefixes every element to its appropriate namespace, given a JAR file 
 * containing all JAXB types that describe the XML sample. 
 * 
 * @author Yvon Comeau
 */
public interface Tartan {

	/**
	 * Converts the XML to valid XML with namespace declarations and prefixes.
	 * 
	 * @param xml  the raw XML to convert
	 * @param jaxbFileName  the full path to the JAR file containing the JAXB classes to use for namespacing the XML
	 * 
	 * @return  the namespaced XML
	 * 
	 * @throws TartanServiceException
	 */
	public abstract String process(String xml, String jaxbFileName) throws TartanServiceException;
	
	/**
	 * Converts the XML to valid XML with namespace declarations and prefixes.
	 * 
	 * @param xml  the raw XML to convert
	 * @param remoteLibraries  a list of URLs for JAR files containing JAXB classes and third party libraries required to compile those classes
	 * 
	 * @return  the namespaced XML
	 * 
	 * @throws TartanServiceException
	 */
	public abstract String process(String xml, List<String> remoteLibraries) throws TartanServiceException;
}