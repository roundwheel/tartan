package ca.roundwheel.tools.xml;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

/**
 * {@code PackageToNamespaceConverter} converts standard java class package into a URL format typical of 
 * XML namespaces.
 * 
 * @author Yvon Comeau
 */
public class PackageToNamespaceConverter {

	/**
	 * The regular expression for a standard Java package where we want to pull out the first two parts to construct 
	 * a URL.  Eg.  com.sun.java -> http://sun.com/java
	 */
	private Pattern PACKAGE_PATTERN = Pattern.compile("([a-z][a-z0-9_]*)\\.([a-z0-9_]+.*?)\\.?(.*?)$");
	
	/**
	 * This is for packages like ca.bell.software.issw.telecom.pack.v1_0_0.lite.businessobject.common
	 * which should be converted to http://www.bell.ca/software/issw/telecom/pack/v1.0.0/lite/businessobject/common
	 */
	private Pattern VERSION_EXCEPTION_PATTERN = Pattern.compile("(.*)/(v\\d+_\\d+_\\d+)/(.*)$");
	
	/**
	 * This is for packages like com.ibm.telecom.v8_5_0.businessobject.system.sid_v12_5.common
	 * which should be converted to http://www.ibm.com/telecom/v8.5.0/businessobject/system/sid-v12.5/common
	 */
	private Pattern VERSION_EXCEPTION2_PATTERN = Pattern.compile("(.*)/(\\D+_v)(\\d+_\\d+)/(.*)$");
	
	/**
	 * This is for packages like com.ibm.software.issw.telecom.pack.v8_5_0.lite.businessobject.system.sid_v12
	 * which should be converted to http://www.ibm.com/software/issw/telecom/pack/v8.5.0/lite/businessobject/system/sid-v12.5
	 */
	private Pattern VERSION_EXCEPTION3_PATTERN = Pattern.compile("(.*)/(\\D+_v)(\\d+)$");
	
	/**
	 * The prefix for packages containing default java types (java.lang, java.util, javax.xml, ...)
	 */
	private static final String JAVA_PACKAGE_PREFIX = "java";
	
	/**
	 * The schema to use when encountering Java types like String, integer, ...
	 */
	private static final String DEFALUT_SCHEMA_PACKAGE = "http://www.w3.org/2001/XMLSchema";
	
	/**
	 * Converts the standard Java package format into the format used in XML namespace declarations.
	 * 
	 * @param pkg  the name of the Java package to convert
	 * 
	 * @return  the package in a format typical of XML namespace declarations
	 */
	public String convertPackageToNamespace(String pkg) {
		
		String result = pkg;
		
		if (!StringUtils.isEmpty(pkg)) {
			
			if (pkg.toLowerCase().startsWith(JAVA_PACKAGE_PREFIX)) {
				result = DEFALUT_SCHEMA_PACKAGE;
			} else {
				Matcher packageMatcher = PACKAGE_PATTERN.matcher(pkg);
				if (packageMatcher.matches()) {
					result = "http://www." + packageMatcher.group(2) + "." + packageMatcher.group(1) + "/" + packageMatcher.group(3).replaceAll("\\.", "/");
				} else {
					result = "http://" + pkg.toUpperCase();	
				}
				// Fix exception cases around version numbers
				Matcher exceptionMatcher = VERSION_EXCEPTION_PATTERN.matcher(result);
				if (exceptionMatcher.matches()) {
					result = exceptionMatcher.group(1) + "/"
							+ exceptionMatcher.group(2).replaceAll("_", "\\.") + "/"
							+ exceptionMatcher.group(3);
				}
				Matcher exception2Matcher = VERSION_EXCEPTION2_PATTERN.matcher(result);
				if (exception2Matcher.matches()) {
					result = exception2Matcher.group(1)   + "/"
							+ exception2Matcher.group(2).replaceAll("_", "-")
							+ exception2Matcher.group(3).replaceAll("_", "\\.") + "/"
							+ exception2Matcher.group(4);
				}
				// TODO - Handle this case as a more generic exception handler
				Matcher exception3Matcher = VERSION_EXCEPTION3_PATTERN.matcher(result);
				if (exception3Matcher.matches()) {
					result = exception3Matcher.group(1)   + "/"
							+ exception3Matcher.group(2).replaceAll("_", "-")
							+ exception3Matcher.group(3).replaceAll("_", "\\.") + ".5";
				}
				
				
			}
		}
		
		return result;
	}
}