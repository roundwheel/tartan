package ca.roundwheel.tools.xml;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * 
 * This implementation was initially inspired by this post:
 * 
 * http://www.java2s.com/Code/Java/XML/Setanamespaceprefixonanelementifitisnotsetalready.htm
 * 
 * @author Yvon Comeau
 */
public class XmlNamespaceFixer {

	static final Logger log = LoggerFactory.getLogger(XmlNamespaceFixer.class);
	
	/**
	 * The prefix to use for all XML elements while the file is being corrected. A unique name is used to 
	 * ensure that we don't inadvertently rename a field not already prefixed.
	 */
	private static final String WORKING_PREFIX = "YZYZYZ";
	
	/**
	 * The prefix to use for all XML elements.  A incremented number will be added to this.
	 */
	private static final String FINAL_PREFIX = "m";
	
	/**
	 * The object/URI map that contains all possible types that could be used in the XML document we want to 
	 * fix.
	 */
	private NamespaceMap namespaceMap;
	
	/**
	 * Creates a new instance of an {@code XmlNamespaceFixer} using the namespace map for the JAXB classes
	 * that support the types used in the XML document.
	 * 
	 * @param namespaceMap
	 */
	public XmlNamespaceFixer(NamespaceMap namespaceMap) {
		this.namespaceMap = namespaceMap;
	}

	/**
	 * This is used to keep track of whether or not a namespace URI has already been used in this document.
	 */
	private Set<String> uriSet = new LinkedHashSet<String>();
	
	/**
	 * Traverses through the XML document and tags each element with the appropriate namespace, while adding
	 * the associated namespace declaration to the root level element.
	 * 
	 * @param xml  the XML to correct
	 * 
	 * @return  the properly namespaced XML
	 * 
	 * @throws TartanServiceException
	 */
	public String tagXmlWithNamespaces(String xml) throws TartanServiceException {
		try {
			
			String fixedXml = fixXml(xml);
			log.debug("fixedXml = " + fixedXml);
			
			Document originalDocument = convertXmlDocument(fixedXml);
			
			Element documentRootElement = originalDocument.getDocumentElement();
			
			String rootNodeName = documentRootElement.getNodeName();
			
			Element newRootElement = originalDocument.createElementNS("http://www.acme.com/schemas", rootNodeName);
			
			String localName = newRootElement.getLocalName();
			
			// Set the desired namespace and prefix
			String rootPrefix = WORKING_PREFIX + "1";
			newRootElement.setPrefix(rootPrefix);

			NodeList topLevelNodeList = documentRootElement.getChildNodes();

			String rootUri = namespaceMap.findRootURI(localName);
			uriSet.add(rootUri);
			
			prefixElements(localName, originalDocument, topLevelNodeList, documentRootElement);
			
			if (rootUri != null) {
    		    		documentRootElement.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:" + rootPrefix, rootUri);
    		    		originalDocument.renameNode(documentRootElement, rootUri, rootPrefix + ":" + rootNodeName);
			}
			
			// Write the content into xml file
			log.debug("Writing new XML document");
			String prefiedXml = convertXmlDocumentToString(originalDocument);
			
			return correctPrefix(prefiedXml);
			
		} catch (DOMException | IOException | ParserConfigurationException | SAXException | TransformerFactoryConfigurationError | TransformerException e) {
			throw new TartanServiceException(e);
		}
	}

	/**
	 * Replaces the working prefix used during processing with a more frienly and short prefix.
	 * 
	 * @param prefixedXml  the XML contents
	 * @return  the corrected XML
	 */
	private String correctPrefix(String prefixedXml) {
		return prefixedXml.replaceAll(WORKING_PREFIX, FINAL_PREFIX);
	}

	/**
	 * Removed leading and trailing double quotes from the input XML.
	 * 
	 * @param xml the XML to correct
	 * @return  the corrected XML
	 */
	private String fixXml(String xml) {
		if (xml.startsWith("\"")) {
			xml = xml.substring(1, xml.length());
		}
		if (xml.endsWith("\"")) {
			xml = xml.substring(0, xml.length() - 1);
		}
		return xml;
	}

	/**
	 * Adds the appropriate prefix to the current XML element, and the namespace declaration to the document
	 * root.
	 * 
	 * @param parentElementPath  the full path down the XML tree to the current element we are fixing
	 * @param originalDocument  the original source XML Document
	 * @param nodeList  the list of XML nodes for the level we are currently processing
	 * @param documentRootElement  the root of the Document we are processing
	 * 
	 * TODO - do we need {@code documentRootElement} as a method parameter - derive it inside the method
	 */
	private void prefixElements(String parentElementPath, Document originalDocument, final NodeList nodeList, Element documentRootElement) {
		
		if (nodeList != null) {
			for (int i = 0; i < nodeList.getLength(); i++) {
				
				final Node node = nodeList.item(i);
			    if (node != null) {
			    		
			    		if (node.getNodeType() == Node.ELEMENT_NODE) {
			    			String nodeName = node.getNodeName();
			    			// FIXME - i'm very tired
				    		String fullElementPath = parentElementPath + "." + nodeName.substring(nodeName.indexOf(":") + 1, nodeName.length());
				    		log.debug("full node path == " + fullElementPath);
				    	  	fullElementPath = fullElementPath.replaceAll(WORKING_PREFIX + "[0-9]*", "");
				    	  	log.debug("full node path fixed == " + fullElementPath);
			    			String uri = namespaceMap.findNamespaceURI(parentElementPath);
	
				    	  	// Recursively fix child nodes
				    	  	NodeList children = node.getChildNodes();
				    	  	prefixElements(fullElementPath, originalDocument, children, documentRootElement);
			    	  	
				    	  	int prefixCount = -1;
				    	  	
				    	  	if (!StringUtils.isEmpty(uri)) {
				    	  		if (uriSet.contains(uri)) {
				    	  			// No need to add this to the root element, we've already done that
				    	  			prefixCount = getSchemaIndex(uri);
				    	  		} else {
					    	  		uriSet.add(uri);
					    	  		prefixCount = uriSet.size();
					    		    documentRootElement.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:" + WORKING_PREFIX + prefixCount, uri);
				    	  		}
				    	  		String prefix = WORKING_PREFIX + prefixCount;
				    	  		log.debug(prefix + " ... " + fullElementPath + " -> " + uri);
					    	  	originalDocument.renameNode(node, uri, prefix + ":" + node.getNodeName());
				    	  	} else {
				    	  		log.warn(fullElementPath + " not found");
				    	  	}
				    }
				}
			}	
		}
	}
	
	/**
	 * Converts the XML String into an w3c {@code Document} object.
	 * 
	 * @param xml  the XML as a String
	 * 
	 * @return  the XML as a w3c {@code Document} object
	 * 
	 * @throws IOException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 */
	private Document convertXmlDocument(String xml) throws IOException, ParserConfigurationException, SAXException {
		
		log.debug("Converting XML to Document:" + xml);
		
		DocumentBuilder builder = createDocumentBuilder();  
		Document document = builder.parse(new InputSource(new StringReader(xml)));
		return document;
	}

	/**
	 * Creates a {@code DocumentBuilder}
	 * 
	 * @return  a new {@code DocumentBuilder} instance.
	 * 
	 * @throws ParserConfigurationException
	 */
	private DocumentBuilder createDocumentBuilder() throws ParserConfigurationException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(false);
		factory.setFeature("http://xml.org/sax/features/namespaces", false);
	    factory.setFeature("http://xml.org/sax/features/validation", false);
		DocumentBuilder builder = factory.newDocumentBuilder();
		return builder;
	}

	/**
	 * Converts a w3c {@code Document} representation of XML back to a String.
	 * 
	 * @param document  the XML as a w3c {@code Document} object
	 * 
	 * @return  the XML as a String
	 * 
	 * @throws TransformerFactoryConfigurationError
	 * @throws TransformerConfigurationException
	 * @throws TransformerException
	 */
	private String convertXmlDocumentToString(Document document)
			throws TransformerFactoryConfigurationError, TransformerConfigurationException, TransformerException {
		
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
		transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
		transformer.setOutputProperty(OutputKeys.INDENT, "no");
		DOMSource source = new DOMSource(document);
		StringWriter writer = new StringWriter();
		
		transformer.transform(source, new StreamResult(writer));
		return writer.getBuffer().toString();
	}
	
	/**
	 * This returns the position of the schema in the set of namespaces that we have collected so far.
	 * This implementation depends on a set that preserves insertion order, such as {@code LinkedHashSet}.
	 * 
	 * @param schema  the schema we are looking for
	 * @return  the schema's position in the set
	 */
	private int getSchemaIndex(String schema) {
		int result = 0;
		for (String entry:uriSet) {
			if (entry.equals(schema)) {
				return result + 1;
			}
			result++;
		}
		return -1;
	}
}