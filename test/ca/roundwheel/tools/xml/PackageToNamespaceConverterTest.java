package ca.roundwheel.tools.xml;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * {@code PackageToNamespaceConverterTest} tests {@code PackageToNamespaceConverter}.
 * 
 * @author Yvon Comeau
 */
public class PackageToNamespaceConverterTest {

	PackageToNamespaceConverter converter = new PackageToNamespaceConverter();
	
	@Test
	public void testConvertPackageToNamespaceWithNullInput() {
		assertNull(converter.convertPackageToNamespace(null));
	}

	@Test
	public void testConvertPackageToNamespaceWithEmptyInput() {
		assertEquals("", converter.convertPackageToNamespace(""));
	}

	// ca.roundwheel.tools.xml
	@Test
	public void testConvertPackageToNamespaceWithStandardInput() {
		assertEquals("http://www.roundwheel.ca/tools/xml", converter.convertPackageToNamespace("ca.roundwheel.tools.xml"));
	}
	
	// tp_system_view_library.roundwheel
	@Test
	public void testConvertPackageToNamespaceWithSingleDotInput() {
		assertEquals("http://www.roundwheel.tp_system_view_library/", converter.convertPackageToNamespace("tp_system_view_library.roundwheel"));
	}
	
	// tpcmn
	@Test
	public void testConvertPackageToNamespaceWithSingleWordInput() {
		assertEquals("http://TPCMN", converter.convertPackageToNamespace("tpcmn"));
	}
	
	// java.lang
	@Test
	public void testConvertPackageToNamespaceWithJavaLang() {
		//assertEquals("http://www.w3.org/2001/XMLSchema", converter.convertPackageToNamespace("java.lang"));
		assertEquals("http://www.w3.org/2001/XMLSchema", converter.convertPackageToNamespace("java.lang"));
	}
	
	// javax...
	@Test
	public void testConvertPackageToNamespaceWithJavax() {
		assertEquals("http://www.w3.org/2001/XMLSchema", converter.convertPackageToNamespace("javax.xml"));
	}
	
	// http://www.roundwheel.ca/telecom/v8.5.0/businessobject/system/sid-v12.5/common
	@Test
	public void testConvertPackageToNamespaceWithSidV12Common() {
		assertEquals("http://www.roundwheel.ca/telecom/v8.5.0/businessobject/system/sid-v12.5/common", converter.convertPackageToNamespace("ca.roundwheel.telecom.v8_5_0.businessobject.system.sid_v12_5.common"));
	}
	
	// http://www.ibm.com/telecom/v8.5.0/businessobject/system/sid-v12.5/common
	@Test
	public void testConvertPackageToNamespaceWithIBMSidV12Common() {
		assertEquals("http://www.ibm.com/telecom/v8.5.0/businessobject/system/sid-v12.5/common", converter.convertPackageToNamespace("com.ibm.telecom.v8_5_0.businessobject.system.sid_v12_5.common"));
	}
	
	// http://www.ibm.com/software/issw/telecom/pack/v8.5.0/lite/businessobject/system/common/extension
	@Test
	public void testConvertPackageToNamespaceWithIBMSidV12CommonExtension() {
		assertEquals("http://www.ibm.com/software/issw/telecom/pack/v8.5.0/lite/businessobject/system/common/extension", converter.convertPackageToNamespace("com.ibm.software.issw.telecom.pack.v8_5_0.lite.businessobject.system.common.extension"));
	}
	
	// http://www.roundwheel.ca/software/issw/telecom/pack/v1.0.0/lite/businessobject/common
	@Test
	public void testConvertPackageToNamespaceWithTelcoPackV1() {
		assertEquals("http://www.roundwheel.ca/software/issw/telecom/pack/v1.0.0/lite/businessobject/common", converter.convertPackageToNamespace("ca.roundwheel.software.issw.telecom.pack.v1_0_0.lite.businessobject.common"));
	}
	
	// http://www.roundwheel.ca/software/issw/telecom/pack/v1.0.0/lite/businessobject/system/sid-v16.5
	@Test
	public void testConvertPackageToNamespaceWithIBMSidV12EndOfPackage() {
		assertEquals("http://www.roundwheel.ca/software/issw/telecom/pack/v1.0.0/lite/businessobject/system/sid-v16.5", converter.convertPackageToNamespace("ca.roundwheel.software.issw.telecom.pack.v1_0_0.lite.businessobject.system.sid_v16"));
	}
	
	// http://www.ibm.com/software/issw/telecom/pack/v8.5.0/lite/businessobject/system/sid-v12.5
	@Test
	public void testConvertPackageToNamespaceWithTelcoPackV1EndOfPackage() {
		assertEquals("http://www.ibm.com/software/issw/telecom/pack/v8.5.0/lite/businessobject/system/sid-v12.5", converter.convertPackageToNamespace("com.ibm.software.issw.telecom.pack.v8_5_0.lite.businessobject.system.sid_v12"));
	}
}
